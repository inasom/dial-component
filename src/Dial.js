import React, { Component } from 'react';

type Props = {
  dialThread?: number,
  spacing?: number,
  onChangeDegree?: (degree: number) => any
}

export default class Dial extends Component<Props> {
  static defaultProps = {
    dialThread: 30,
    spacing: 30,
    onChangeDegree: () => {},
  }

  state = {
    degree: 0,
  }

  dialRef = React.createRef()

  componentWillUnmount() {
    window.removeEventListener('mousemove', this.traceDial);
    window.removeEventListener('mouseup', this.endTraceDial);
  }

  componentDidUpdate(prevProps, prevState) {
    const { degree } = this.state;
    if (degree !== prevState.degree) {
      this.props.onChangeDegree(degree);
    }
  }

  startTraceDial = (event) => {
    this.setState({
      tracing: true,
    });
    this.calculateAngle(event);

    window.addEventListener('mousemove', this.traceDial);
    window.addEventListener('mouseup', this.endTraceDial);
  }

  traceDial = (event, isAuto) => {
    const { dialThread } = this.props;
    if (!this.threading) {
      this.calculateAngle(event);
      this.threading = true;
      setTimeout(() => {
        this.threading = false;
      }, dialThread);
    } else if (isAuto) {
      this.checkDegreeDiff(event);
    }
  }

  endTraceDial = (event) => {
    this.setState({
      tracing: false,
    });
    window.removeEventListener('mousemove', this.traceDial);
    window.removeEventListener('mouseup', this.endTraceDial);
  }

  calculateAngle = (event) => {
    clearTimeout(this.checkDegreeDiffTimeout);
    const { spacing } = this.props;
    const { degree } = this.state;
    const dial = this.dialRef.current;
    const dialRect = dial.getBoundingClientRect();
    const originX = dialRect.x + dialRect.width / 2;
    const originY = dialRect.y + dialRect.height / 2
    const eventX = originX - event.clientX;
    const eventY = originY - event.clientY;
    const cursorDegree = this.absDegree(Math.atan2(eventX, eventY) * -180 / Math.PI);

    let degreeDiff = cursorDegree - degree;

    console.log('Diff:', degreeDiff, ' Cursor:', cursorDegree, ' Dial:', degree);
    
    if (degreeDiff < -180) {
      degreeDiff = degreeDiff + 360;
    } else if (degreeDiff > 180) {
      degreeDiff = degreeDiff - 360;
    }

    const absDiff = Math.abs(degreeDiff)

    if (absDiff > spacing / 2) {
      this.setState({
        degree: this.convertDegreeRange(degree + absDiff / degreeDiff * spacing),
      });
      this.checkDegreeDiff(event);
    }
  }

  convertDegreeRange = (degree) => {
    if (degree >= 360) {
      return 360 - degree;
    } else if (degree < 0) {
      return 360 + degree;
    }
    return degree;
  }

  checkDegreeDiff = (event) => {
    const { dialThread } = this.props;
    const { clientX, clientY } = event;
    this.checkDegreeDiffTimeout = setTimeout(() => this.traceDial({ clientX, clientY }, true), dialThread);
  }

  absDegree = (degree) => degree > 0 ? degree : 360 + degree

  render() {
    const { degree } = this.state;
    return (
      <div style={{ transform: `rotate(${degree}deg)`}} ref={this.dialRef} className="dial-wrapper" onMouseDown={this.startTraceDial}>
        <div className="dial-inner">
        </div>
      </div>
    )
  }
}