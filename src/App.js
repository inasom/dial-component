import React, { Component } from 'react';
import Dial from './Dial';
import './App.css';

class App extends Component {
  state = {
    degree: 0,
  }

  render() {
    return (
      <div className="App">
        <Dial onChangeDegree={(degree) => this.setState({ degree })}/>
        <p className="app-degree-counter">{this.state.degree}</p>
      </div>
    );
  }
}

export default App;
